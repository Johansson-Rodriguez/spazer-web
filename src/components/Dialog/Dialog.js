import React from 'react';
import { Dialog, DialogTitle, DialogContent, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    title: {
        fontSize: 18,
    }
}));

const Dialogs = ({ open, onClose, title, children }) => {

    const classes = useStyles()

    return (
        <Dialog
            onClose={onClose}
            fullWidth={true}
            maxWidth={'lg'}
            aria-labelledby="customized-dialog-title"
            open={open}
        >
            <DialogTitle
                id="customized-dialog-title"
                onClose={onClose}>
                <Typography className={classes.title}>{title}</Typography>
            </DialogTitle>
            <DialogContent dividers style={{ padding: 25 }}>
                {children}
            </DialogContent>
        </Dialog>
    )
}

export default Dialogs;