import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Stepper, Step, StepLabel} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    backButton: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
}));

const Steppers = ({ steps, components, activeStep }) => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Stepper activeStep={activeStep} alternativeLabel>
                {steps.map((label) => (
                    <Step key={label}>
                        <StepLabel>{label}</StepLabel>
                    </Step>
                ))}
            </Stepper>
            <div>
                {components[activeStep]}
            </div>
        </div>
    )
}

export default Steppers;