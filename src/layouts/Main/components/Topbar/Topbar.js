import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme } from '@material-ui/styles';
import { AppBar, Toolbar, Hidden, IconButton, useMediaQuery } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';

import { Logout, Items, InBox, Menu } from './Components';

const useStyles = makeStyles({
  appBar: props => ({
    zIndex: props.elevation ? props.elevation : 5,
    backgroundColor: props.typeUser !== 2 ? 'black' : 'white'
  }),
  flexGrow: props => ({
    flexGrow: 1,
    color: props.typeUser === 2 ? 'black' : 'white'
  }),
  toolbar: {
    paddingBottom: 10
  }
});

const Topbar = ({
  appIconStyle,
  appLeftIcon,
  elevation,
  className,
  onSidebarOpen,
  children,
  ...rest
}) => {

  const typeUser = parseInt(JSON.parse(localStorage.getItem('spazer_user')).role);
  
  const classes = useStyles({ elevation, typeUser });

  const [notifications] = useState([]);

  

  const theme = useTheme();
  const isDesktop = useMediaQuery(theme.breakpoints.up('lg'), {
    defaultMatches: true
  });

  return (
    <AppBar {...rest} className={classes.appBar}>
      <Toolbar className={classes.toolbar}>

        {
          typeUser !== 2 && (
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '100%'
              }}>
              <img
                alt="jogo-indicator"
                src={require('assets/images/spazer_1.png')}
                style={{ objectFit: 'contain', height: 50 }}
              />
            </div>
          )
        }

        {
          isDesktop && (
            <Items />
          )
        }

        <div className={classes.flexGrow} />

        {
          typeUser === 2 && (
            <Hidden mdDown>
              {!children && <Logout numberOfNotificacions={notifications.length} />}
            </Hidden>
          )
        }

        {
          typeUser !== 2 && (
            <>
              <InBox />
              <Menu />
            </>
          )
        }

        <Hidden lgUp>
          <IconButton onClick={onSidebarOpen}>
            <MenuIcon />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
};

Topbar.propTypes = {
  className: PropTypes.string,
  onSidebarOpen: PropTypes.func
};

export default Topbar;
