import React from 'react';
import { Badge, makeStyles, Popper, Paper } from '@material-ui/core';
import { IconsMaterial } from 'icons';
import { Container, Footer, Header, Target } from './Components';

const useStyles = makeStyles({
    Badge: {
        marginRight: 20,
        '&:hover': {
            color: 'white',
            cursor: 'pointer'
        }
    },
    Popper: {
        marginTop: 30,
        position: 'fixed',
        zIndex: 2000
    },
    Paper: {
        // marginTop: 30,
    },
    active: {
        color: 'white',
        marginRight: 20,
        '&:hover': {
            cursor: 'pointer'
        }
    }
})

const InBox = () => {

    const classes = useStyles();

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(anchorEl ? null : event.currentTarget);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popper' : undefined;

    return (
        <>
            <Badge badgeContent={4} color="primary" className={anchorEl ? classes.active : classes.Badge} onClick={handleClick}>
                {IconsMaterial.InBox}
            </Badge>
            <Popper
                id={id}
                open={open}
                anchorEl={anchorEl}
                className={classes.Popper}
            >
                <Paper 
                    className={classes.Paper}
                >
                    <Container>
                        <Header />
                        <Target />
                        <Target />
                        <Target />
                        <Target />
                        <Target />
                        <Footer />
                    </Container>
                </Paper>

            </Popper>
        </>
    )
}

export default InBox;