import { makeStyles } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles({
    header: {
        width: '100%',
        height: 60,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

const Header = () => {
    
    const classes = useStyles();
    
    return (
        <div className={classes.header}>
            Notificaciones
        </div>
    )
}

export default Header;