import React from 'react';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
    Target: {
        borderTop: '1px solid rgba(0,0,0,0.3)',
        borderBottom: '1px solid rgba(0,0,0,0.3)',
        height: 'auto',
        padding: 15
    },
    label: {
        fontSize: 14
    },
    title: {
        fontSize: 16
    }
})

const Target = () => {

    const classes = useStyles();

    const str = "Ha reservado a cancha #2 para las 3:00 con reserva de $5".split('')

    return (
        <div className={classes.Target}>
            <h4 className={classes.title}>Miguel Villegas</h4>
            <p className={classes.label}>
                {
                    str.length >= 51 ? str.splice(0, 48).join('') + '...' : str.join('')
                }
            </p>
        </div>
    )
}

export default Target;