import { makeStyles } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles({
    footer: {
        width: '100%',
        height: 60,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        '&:hover': {
            cursor: 'pointer',
            background: 'rgba(0,0,0,0.2)',
            transition: '1s'
        }
    }
})

const Footer = () => {
    const classes = useStyles();
    
    return (
        <div className={classes.footer} onClick={() => alert('hola')}>
            Ver todas
        </div>
    )
}

export default Footer;