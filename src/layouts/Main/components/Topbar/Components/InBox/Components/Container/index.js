import { makeStyles } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles({
    container: {
        width: '350px',
        height: 'max-content'
    }
})

const Container = ({ children }) => {

    const classes = useStyles()

    return (
        <div className={classes.container}>
            {children}
        </div>
    )
}

export default Container;