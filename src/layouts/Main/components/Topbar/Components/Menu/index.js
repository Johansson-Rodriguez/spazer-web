import { Button, Popper, Grow, Paper, ClickAwayListener, MenuList, MenuItem, makeStyles, Hidden } from '@material-ui/core';
import { IconsMaterial } from 'icons';
import React from 'react'


const useStyles = makeStyles({
    button: {
        width: 'max-content',
        color: 'white'
    },
    twoElements: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    Paper: {
        marginTop: 20
    }
})

const Menu = () => {

    const classes = useStyles();

    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef(null);

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }

        setOpen(false);
    };

    function handleListKeyDown(event) {
        if (event.key === 'Tab') {
            event.preventDefault();
            setOpen(false);
        }
    }

    // return focus to the button when we transitioned from !open -> open
    const prevOpen = React.useRef(open);
    React.useEffect(() => {
        if (prevOpen.current === true && open === false) {
            anchorRef.current.focus();
        }

        prevOpen.current = open;
    }, [open]);

    return (
        <Hidden mdDown>
            <div>
                <Button
                    ref={anchorRef}
                    aria-controls={open ? 'menu-list-grow' : undefined}
                    aria-haspopup="true"
                    onClick={handleToggle}
                    className={classes.button}
                >
                    Gambeta los proceres {IconsMaterial.Expand}
                </Button>
                <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
                    {({ TransitionProps, placement }) => (
                        <Grow
                            {...TransitionProps}
                            style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                        >
                            <Paper className={classes.Paper}>
                                <ClickAwayListener onClickAway={handleClose}>
                                    <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                                        <MenuItem onClick={handleClose}>Configuración de la cuenta</MenuItem>
                                        <MenuItem className={classes.twoElements} onClick={handleClose}>Cerrar Sesión <span>{IconsMaterial.Power}</span></MenuItem>
                                    </MenuList>
                                </ClickAwayListener>
                            </Paper>
                        </Grow>
                    )}
                </Popper>
            </div>
        </Hidden>
    )
}

export default Menu;