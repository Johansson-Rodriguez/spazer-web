import React, { useState, useEffect } from 'react'

import { makeStyles } from '@material-ui/styles';
import { FormControlLabel, Grid, Typography, Checkbox, TextField } from '@material-ui/core';
import { Button, Container } from 'components';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(4)
    },
    name: {
        marginTop: theme.spacing(1),
        color: 'black'
    },
    createBranchContainer: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    containerButton: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: 20,
        width: '100%'
    }
}));

const ServicesSettings = () => {

    const classes = useStyles();

    const [service, setService] = useState({
        active: false,
        duration: '',
        name: '',
        price: ''
    })

    const handleChange = ({ target: { name, value, checked } }) => {
        if (name === "active") {
            setService({ ...service, active: checked })
        } else {
            setService({ ...service, [name]: value })
        }
    };

    useEffect(() => {
        
    }, [])

    return (
        <div className={classes.root}>
            <Grid container>
                <Grid item md={6} sm={6} xs={12}>
                    <Typography className={classes.name} variant="h4">
                        Nombre del servicio / Nombre de sucursal
                    </Typography>
                </Grid>
                <Grid
                    item
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.createBranchContainer}
                >
                    <FormControlLabel
                        value={service.active}
                        control={<Checkbox color="primary" checked={service.active} />}
                        label="¿Activo?"
                        labelPlacement="start"
                        name="active"
                        onChange={handleChange}
                    />
                </Grid>
            </Grid>
            <Container>
                <Grid container spacing={2}>
                    <Grid item md={6} xs={12}>
                        <TextField
                            fullWidth
                            type="text"
                            label="Nombre del servicio"
                            margin="dense"
                            name="name"
                            onChange={handleChange}
                            required
                            value={service.name}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item md={6} xs={12}>
                        <TextField
                            fullWidth
                            type="text"
                            label="Duración"
                            margin="dense"
                            name="duration"
                            onChange={handleChange}
                            required
                            value={service.duration}
                            variant="outlined"
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item md={6} xs={12}>
                        <TextField
                            fullWidth
                            type="text"
                            label="Precio del servicio"
                            margin="dense"
                            name="price"
                            onChange={handleChange}
                            required
                            value={service.price}
                            variant="outlined"
                        />
                    </Grid>
                </Grid>
            </Container>
            <div className={classes.containerButton}>
                <Button
                    subtitle="Guardar"
                    type="primary"
                />
            </div>
        </div>
    )
}

export default ServicesSettings;