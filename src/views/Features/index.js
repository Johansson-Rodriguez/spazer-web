import React from 'react';

import { makeStyles } from '@material-ui/styles';

import { useGet } from 'hooks/useGet';
import { Loading, Button, Table } from 'components';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  },
  containerButtons: {
    display: 'flex'
  }
}));

const Features = () => {
  const classes = useStyles();

  const [loading, features] = useGet('/feature/getAll');

  const history = useHistory()

  const handleClickEdit = ({ row }) => history.push(`/features/edit/${row.id}`);

  const handleClickCreate = () => history.push(`/features/creation`)

  return (
    <div className={classes.root}>
      {
        loading ? (
          <Loading />
        ) : (
          <>
            <div className={classes.containerButtons}>
              <Button
                subtitle="Crear caracteristicas"
                type="inherit"
                onClick={handleClickCreate}
              />
              <Button
                title={features.features.length}
                subtitle="Caracteristicas"
                type="primary"
              />

            </div>
            <Table
              data={features.features}
              type="caracteristicas"
              showDelete={false}
              onClickEdit={handleClickEdit}
            />
          </>
        )
      }
    </div>
  );
  };

  export default Features;
