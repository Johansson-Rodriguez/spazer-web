import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types';

import { useGet } from 'hooks/useGet';

import { Header, Select } from 'views/BranchOfficeSettings/components';

import { Grid, TextField } from '@material-ui/core';
import { Alert, Container } from 'components';

const General = ({ data, onChange, onSubmit, title, buttonText, disabledSelect }) => {

    const [loading, BussinesCategorys] = useGet('/businessCategory/getAllActive');
    const [selectOptions, setSelectOptions] = useState([])
    const [alert, setAlert] = useState({open: false, msg: '', type: 'info'});

    const handleChange = ({ target: { name, value } }) => onChange(name, value);

    const verifyContent = () => {

        let verify = true;
        
        const validatePhone = /^[0-9]{4}-[0-9]{4}$/

        if(!data.typeServices){
            verify = false
            setAlert({open: true, msg: 'No a seleccionado el tipo de servicio', type: 'info'});
        }
        
        if(!data.schedules){
            verify = false
            setAlert({open: true, msg: 'Verifique el horario de la sucursal', type: 'info'});
        }
        
        if(!validatePhone.test(data.phone.trim())) {
            verify = false
            setAlert({open: true, msg: 'Verifique el numero de telefono', type: 'info'});
        }
        
        if(!data.address) {
            verify = false
            setAlert({open: true, msg: 'Verifique la direccion de la sucursal', type: 'info'});
        }
        
        if(!data.name) {
            verify = false
            setAlert({open: true, msg: 'Verifique el nombre de sucursal', type: 'info'});
        }

        return verify;
    }

    const handleSubmit = () => verifyContent() && onSubmit()

    useEffect(() => {

        if (!loading) {
            const newBussinesCategorys = BussinesCategorys.categories.map(({ _id, name }) => ({ label: name, value: _id }));
            setSelectOptions(newBussinesCategorys);
        }

    }, [loading, BussinesCategorys])

    return (
        <>
            <Header
                sectionName={title}
                buttonText={buttonText}
                onClickButton={handleSubmit}
            />
            <Container>
                <Grid container spacing={2}>
                    <Grid item md={6} xs={12}>
                        <TextField
                            fullWidth
                            type="text"
                            label="Nombre de sucursal"
                            margin="dense"
                            name="name"
                            onChange={handleChange}
                            required
                            value={data.name}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item md={6} xs={12}>
                        <TextField
                            fullWidth
                            type="text"
                            label="Dirección de sucursal"
                            margin="dense"
                            name="address"
                            onChange={handleChange}
                            required
                            value={data.address}
                            variant="outlined"
                        />
                    </Grid>
                </Grid>

                <Grid container spacing={2}>
                    <Grid item md={6} xs={12}>
                        <TextField
                            fullWidth
                            type="text"
                            label="Número de teléfono"
                            margin="dense"
                            name="phone"
                            onChange={handleChange}
                            required
                            value={data.phone}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item md={6} xs={12}>
                        <TextField
                            fullWidth
                            type="text"
                            label="Horarios"
                            margin="dense"
                            name="schedules"
                            onChange={handleChange}
                            required
                            value={data.schedules}
                            variant="outlined"
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={2}>

                    <Select
                        title="Tipo de servicio*"
                        name="typeServices"
                        values={selectOptions}
                        onChange={handleChange}
                        value={data.typeServices}
                        disabled={loading || disabledSelect}
                    />
                </Grid>
            </Container>
            <Alert
                open={alert.open}
                title={alert.msg}
                type={alert.type}
                handleClose={() => setAlert({open: false, msg: '', type: 'info'})}
            />
        </>
    )
}

General.propTypes = {
    data: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    title: PropTypes.string,
    buttonText: PropTypes.string,
    disabledSelect: PropTypes.bool
}

General.defaultProps = {
    title: 'Crea una nueva sucursal',
    buttonText: 'Agregar sucursal',
    disabledSelect: false
}

export default General;