import { Grid, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { Alert, Button, Loading, Modal, Container } from 'components';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  EmptySection,
  Header
} from 'views/BranchOfficeSettings/components';
import { useGet } from 'hooks/useGet';

import axiosInstance from 'httpConfig';
import { include } from 'underscore';

const useStyles = makeStyles(() => ({
  contentFeatures: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  contentButton: {
    margin: 10,
    justifyContent: 'center',
    display: 'flex'
  },
  contentButtonAdd: {
    display: 'flex',
    padding: 15,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  }
}));

const Features = ({ type, onAdd, onRemove, initValues }) => {
  const classes = useStyles();

  const initialState = {
    name: '',
    description: ''
  };

  const [state, setState] = useState(initialState); //Modal
  const [isOpenModal, setIsOpenModal] = useState(false); //is Open Modal
  const [features, setFeatures] = useState([]);
  const [loading, featuresGetByID] = useGet(`/feature/getByCategory/${type}`); //Get categorys
  const [alert, setAlert] = useState({
    open: false,
    msg: '',
    type: 'info'
  });

  const handleChange = ({ target: { name, value } }) =>
    setState({ ...state, [name]: value });

  const handleChangeCheck = index => {
    let featuresAux = features.map((feature, i) => {
      if (i === index) {
        return { ...feature, isSelected: !feature.isSelected };
      }
      return { ...feature };
    });

    setFeatures(featuresAux);
  };

  const handleCheck = (index, id) => {
    handleChangeCheck(index);
    onAdd('features', id);
  };

  const handleNotCheck = (index, id) => {
    handleChangeCheck(index);
    onRemove('features', id);
  };

  const handleChangeStateModal = () => type && setIsOpenModal(!isOpenModal);

  const handleAdd = () => {
    const token = localStorage.getItem('spazer_token');

    const headers = {
      'Content-Type': 'application/json',
      'access-token': token
    };

    axiosInstance
      .post('/feature/create', { ...state, categories: [type] }, { headers })
      .then(response => console.log(response.data))
      .catch(err => {
        setAlert({
          open: true,
          msg: 'Error al agregar una caracteristica',
          type: 'error'
        });
        console.error(err);
      });
    // onAdd('features', { ...state, id: data.length });
    // setState(initialState);
    // handleChangeStateModal()
  };

  useEffect(() => {

    if (!loading && type && featuresGetByID.features) {
      
      let includeElements = []

      for (let i = 0; i < initValues.length; i++) {
        includeElements.push(featuresGetByID.features.findIndex((el) => el._id === initValues[i]))
      }

      let featuresArr = [...featuresGetByID.features];

      includeElements.map(val => {
        featuresArr[val] = {...featuresArr[val], isSelected: true }

        return val;
      })

      setFeatures(featuresArr);

    }


  }, [loading, type]);

  return (
    <>
      <Header
        sectionName="Caracteristicas"
        buttonText="Agregar caracteristica"
        isActiveMargin={true}
        onClickButton={handleChangeStateModal}
      />

      <Container>
        {loading && <Loading />}

        {!loading && (
          <>
            {features.length === 0 && (
              <EmptySection title="No se han agregado caracteristicas" />
            )}

            {features.length > 0 && (
              <div className={classes.contentFeatures}>
                {features.map(({ name, isSelected, _id }, i) => (
                  <div
                    key={`features_${name}_${i}`}
                    className={classes.contentButton}>
                    <Button
                      subtitle={name}
                      type={isSelected ? 'primary' : 'inherit'}
                      onClick={() => {
                        if (isSelected) {
                          handleNotCheck(i, _id);
                        } else {
                          handleCheck(i, _id);
                        }
                      }}
                    />
                  </div>
                ))}
              </div>
            )}
          </>
        )}
      </Container>

      <Modal
        open={isOpenModal}
        onClose={handleChangeStateModal}
        title="Creación de característica">
        <Grid container spacing={1}>
          <Grid item md={6} xs={12}>
            <TextField
              fullWidth
              type="text"
              label="Nombre de carácteristica"
              margin="dense"
              name="name"
              required
              variant="outlined"
              value={state.name}
              onChange={handleChange}
            />
          </Grid>
          <Grid item md={6} xs={12}>
            <TextField
              fullWidth
              type="text"
              label="Descripcion"
              margin="dense"
              name="description"
              required
              variant="outlined"
              value={state.code}
              onChange={handleChange}
            />
          </Grid>
        </Grid>
        <div className={classes.contentButtonAdd}>
          <Button
            subtitle="Agregar Característica"
            type="primary"
            onClick={handleAdd}
          />
        </div>
      </Modal>
      <Alert
        open={alert.open}
        handleClose={() => setAlert({ error: false, msg: '', type: 'info' })}
        title={alert.msg}
        type={alert.type}
      />
    </>
  );
};

Features.propTypes = {
  onAdd: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
  initValues: PropTypes.array
};

Features.defaultProps = {
  initValues: []
}

export default Features;
