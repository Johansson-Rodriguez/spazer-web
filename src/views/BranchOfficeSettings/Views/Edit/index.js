import { useGet } from 'hooks/useGet';
import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { Loading } from 'components';
import { Grid } from '@material-ui/core';
import {
    Features,
    General,
    Images,
    Services
} from 'views/BranchOfficeSettings/Sections';
import axiosInstance from 'httpConfig';

const Edit = () => {

    const { id } = useParams();

    const history = useHistory();

    const [loading, branchOfficeData] = useGet(`/branchOffice/${id}`);
    const [branchOffice, setBranchOffice] = useState({
        name: '',
        address: '',
        phone: '',
        schedules: '',
        typeServices: '',
        images: [],
        services: [],
        features: []
    });
    const [nameBranchOffice, setNameBranchOffice] = useState('');
    const [images, setImages] = useState([]);
    const [servicesData, setServicesData] = useState([])

    const handleChangeGeneral = (name, value) =>
        setBranchOffice({ ...branchOffice, [name]: value });

    const handleChangeImage = (url, file) =>
        setImages([...images, { url, file }]);

    const handleRemove = index => {
        const newImages = [...images];

        newImages.splice(index, 1);

        setImages([...newImages]);
    };

    const handleAdd = (to, value) =>
        setBranchOffice({ ...branchOffice, [to]: [...branchOffice[to], value] });

    const handleRemoveOrEdit = (to, id) => {
        const arr = [...branchOffice[to]];

        const newArr = arr.filter(val => val !== id);

        setBranchOffice({ ...branchOffice, [to]: [...newArr] });
    };

    const handleSubmit = () => {
        const {
            _id,
            name,
            address,
            phone,
            schedules,
            typeServices,
            services,
            features
        } = branchOffice;

        const imagesAux = images.map(({ file }) => file);

        const form = new FormData();

        form.append('id', _id);
        form.append('name', name.trim());
        form.append('address', address.trim());
        form.append('phone', phone.trim());
        form.append('schedules', schedules.trim());
        form.append('typeServices', typeServices);

        for (const file of imagesAux) {
            if(typeof file === 'string'){
                form.append('oldImages', file);
            }
            else{
                form.append('images', file);
            }
        }

        for (const service of services) {
            form.append('services', service);
        }

        for (const feature of features) {
            form.append('features', feature);
        }

        const token = localStorage.getItem('spazer_token');

        const headers = {
            'Content-Type': 'multipart/form-data',
            'access-token': token
        };

        axiosInstance
            .put('/branchOffice/update', form, { headers })
            .then(({ data }) => {
                console.log(data);
                history.push('/branch-offices');
            })
            .catch(err => console.error(err));
    };

    useEffect(() => {
        if (!loading) {

            console.log(branchOfficeData)

            setServicesData(branchOfficeData.branchOffice.services);

            const currentServicesIds = branchOfficeData.branchOffice.services.map(val => val._id);
            setBranchOffice({ 
                ...branchOfficeData.branchOffice, 
                schedules: branchOfficeData.branchOffice.schedules.join(', ').trim(),
                services: currentServicesIds
            });

            const { name, images: Images } = branchOfficeData.branchOffice;

            setNameBranchOffice(`Sucursal ${name}`);

            const newArrImages = Images.map(val => ({ file: val, url: val }));

            setImages(newArrImages);
        }
    }, [loading, branchOfficeData]);

    return (
        <>
            {loading && <Loading />}
            {!loading && (
                <Grid container spacing={4}>
                    <General
                        data={branchOffice}
                        title={nameBranchOffice}
                        onChange={handleChangeGeneral}
                        onSubmit={handleSubmit}
                        buttonText="Editar Sucursal"
                        disabledSelect={true}
                    />

                    <Images
                        images={images}
                        onChange={handleChangeImage}
                        onRemove={handleRemove}
                    />

                    <Services
                        initValues={servicesData}
                        onAdd={handleAdd}
                        onRemove={handleRemoveOrEdit}
                    />

                    {branchOffice.typeServices && (
                        <Features
                            type={branchOffice.typeServices}
                            onAdd={handleAdd}
                            onRemove={handleRemoveOrEdit}
                            initValues={branchOffice.features}
                        />
                    )}
                </Grid>
            )}
        </>
    );
};

export default Edit;
