import React, { useRef, useState } from 'react';
import { makeStyles } from '@material-ui/core';
import FullCalendar from '@fullcalendar/react';

// import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';

import './calendar-styles.css';
import { ContentCalendar, NewBooking, SelectedRange } from './Components';

let paletteProvider = '';

const useStyles = makeStyles(theme => {

    paletteProvider = theme.palette;

    return {
        container: {
            paddingLeft: 20,
            paddingRight: 20
        }
    }
})

const Calendar = () => {

    const classes = useStyles();

    const [bookingsByMonth, setBookingsByMonth] = useState([
        {
            start: '2021-07-16T15:00:00',
            end: '2021-07-16T16:00:00',
            hols: 'pepe'
        }
    ]);

    const [newBooking, setNewBooking] = useState({
        open: false,
        date: ''
    })

    const [rangeSelectView, setRangeSelectView] = useState(false)

    const calendarRef = useRef(null);

    const renderEventContent = ({ event }) => <ContentCalendar event={event} />

    const handleViewRangeSelect = e => setRangeSelectView(rangeSelectView ? null : e.currentTarget)
    
    const handleViewModal = (date = '') => setNewBooking({...newBooking, open: !newBooking.open, date})

    const handleDateClick = (arg) => {

        handleViewModal(arg.date)
    }

    return (
        <div className={classes.container}>
            <FullCalendar
                eventColor={paletteProvider.primary.main}
                eventTextColor="black"
                ref={calendarRef}
                contentHeight={600}
                dateClick={handleDateClick}
                eventContent={renderEventContent}
                events={bookingsByMonth}
                headerToolbar={{
                    left: 'prev,next today',
                    center: 'title',
                    right: 'type rangeDate timeGridWeek,timeGridDay'
                }}
                buttonText={{
                    today: 'Hoy',
                    week: 'Semana',
                    day: 'Día'
                }}
                customButtons={{
                    rangeDate: {
                        text: 'Seleccionar Fecha',
                        click: handleViewRangeSelect
                    },
                    type: {
                        text: 'Tipo de reserva',
                        click: () => { }
                    }
                }}
                plugins={[interactionPlugin, timeGridPlugin]}
                locales={['es']}
                weekends={true}
                validRange={{}}
                slotLabelFormat={{
                    hour: 'numeric',
                    minute: '2-digit',
                    // omitZeroMinute: true,
                }}
                eventTimeFormat={{
                    hour: 'numeric',
                    minute: '2-digit',
                    // omitZeroMinute: true,
                }}
                timeZoneParam="h(:mm)t"

            />

            <SelectedRange anchorEl={rangeSelectView} handleClose={handleViewRangeSelect} />

            <NewBooking open={newBooking.open} onClose={handleViewModal} />
        </div>
    )
}

export default Calendar;