import React, { useEffect, useState } from 'react'
import { Dialog, Stepper } from 'components';
import { Step1, Step2, Step3 } from './Steps';

const NewBooking = ({ open, onClose }) => {

    const [activeStep, setActiveStep] = useState(0);

    const checkBackButton = () => {
        
        if (activeStep === 0) return onClose() ;
        
        setActiveStep(activeStep - 1)
        
    }

    const nextButton = () => setActiveStep(activeStep + 1)

    const step = {
        titles: ['Información general', 'Detalles de reserva', 'Resumen de reserva'],
        components: [
            <Step1
                back={checkBackButton}
                next={nextButton}
            />,
            <Step2
                back={checkBackButton}
                next={nextButton}
            />, 
            <Step3
                back={checkBackButton}
                next={nextButton}
            />
        ]
    }

    useEffect(() => {
        if(!open) {
            setActiveStep(0)
        }
    }, [open])

    return (
        <Dialog
            open={open}
            onClose={onClose}
            title="Agregar reserva"
        >
            <Stepper
                steps={step.titles}
                components={step.components}
                activeStep={activeStep}
            />

            
        </Dialog>
    )
}

export default NewBooking;