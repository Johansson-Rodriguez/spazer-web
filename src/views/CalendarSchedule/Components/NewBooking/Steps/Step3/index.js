import React from 'react'
import { DialogActions } from '@material-ui/core';
import { Button } from 'components';

const Step3 = ({ back, next }) => {
    return (
        <div>
            
            <DialogActions style={{ padding: 25 }}>
                <Button
                    subtitle="Anterior"
                    type="inherit"
                    onClick={back}
                />
                <Button
                    subtitle="Reservar"
                    type="primary"
                    onClick={next}
                />
            </DialogActions>
        </div>
    )
}

export default Step3;