import React from 'react'
import { DialogActions, Grid, Typography, RadioGroup, Radio, FormControlLabel, makeStyles } from '@material-ui/core';
import { Button } from 'components';
import { IconsMaterial } from 'icons';

const useStyles = makeStyles(theme => ({
    row: {
        marginBottom: 15
    },
    rowCell: {
        display: 'flex',
        justifyContent: 'flex-start',
        justifyItems: 'center'
    },
    stepLabel: {
        marginBottom: 10
    }
}));

const Step1 = ({ back, next }) => {

    const classes = useStyles();

    return (
        <div>

            <Grid container>
                <Grid item xs={6}>
                    <Typography variant="h3" className={classes.stepLabel}>
                        Información General
                    </Typography>
                    <Grid container spacing={1} className={classes.row}>
                        <Grid item xs={6} className={classes.rowCell}>
                            <Grid container spacing={1}>
                                <Grid
                                    item
                                    md={2}
                                    xs={2}
                                    style={{ display: 'flex', alignItems: 'center' }}>
                                    {IconsMaterial.AccountBox}
                                </Grid>
                                <Grid
                                    item
                                    md={10}
                                    xs={10}
                                    style={{ display: 'flex', alignItems: 'center' }}>
                                    <Typography>Tipo de reserva</Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={6} className={classes.rowCell}>
                            <RadioGroup
                                row
                                aria-label="position"
                                name="position"
                                // onChange={event => props.setTypeOfUser(event.target.value)}
                                // defaultValue={props.typeOfUser}
                            >
                                <FormControlLabel
                                    value="u"
                                    control={<Radio color="primary" />}
                                    label="Usuario"
                                    labelPlacement="top"
                                />
                                <FormControlLabel
                                    value="n"
                                    control={<Radio color="primary" />}
                                    label="Sin usuario"
                                    labelPlacement="top"
                                />
                            </RadioGroup>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>

            <DialogActions style={{ padding: 25 }}>
                <Button
                    subtitle="Cancelar"
                    type="inherit"
                    onClick={back}
                />
                <Button
                    subtitle="Siguiente"
                    type="primary"
                    onClick={next}
                />
            </DialogActions>
        </div>
    )
}

export default Step1;