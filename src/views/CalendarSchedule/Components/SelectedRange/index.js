import React, { useEffect, useState } from 'react';
import Popover from '@material-ui/core/Popover';

import { DateRangePicker, defaultStaticRanges, defaultInputRanges } from 'react-date-range';
import * as rdrLocales from 'react-date-range/dist/locale';
import { addDays } from 'date-fns';
import 'react-date-range/dist/styles.css'; // main css file
import 'react-date-range/dist/theme/default.css'; // theme css file

const SelectedRange = ({ anchorEl, handleClose }) => {

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    const [state, setState] = useState([
        {
            startDate: new Date(),
            endDate: new Date(),
            key: 'selection'
        }
    ])

    const [labelsRanges, setLabelsRanges] = useState({
        inputs: [],
        static: []
    })

    useEffect(() => {
        const inputs = [
            {
                ...defaultInputRanges[0],
                label: 'Días hasta de hoy'
            },
            {
                ...defaultInputRanges[1],
                label: 'Días a partir de hoy'
            }
        ]

        const statics = [
            {
                ...defaultStaticRanges[0],
                label: 'Hoy'
            },
            {
                ...defaultStaticRanges[1],
                label: 'Ayer'
            },
            {
                ...defaultStaticRanges[2],
                label: 'Esta semana'
            },
            {
                ...defaultStaticRanges[3],
                label: 'Semana anterior'
            },
            {
                ...defaultStaticRanges[4],
                label: 'Este mes'
            },
            {
                ...defaultStaticRanges[5],
                label: 'Mes anterior'
            },
        ]

        setLabelsRanges({ inputs, static: statics });

    }, [])

    return (
        <Popover
            id={id}
            open={open}
            anchorEl={anchorEl}
            onClose={handleClose}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
            }}
        >
            <DateRangePicker
                onChange={item => setState([item.selection])}
                showSelectionPreview={true}
                moveRangeOnFirstSelection={false}
                months={2}
                ranges={state}
                direction="horizontal"
                locale={rdrLocales.es}
                staticRanges={labelsRanges.static}
                inputRanges={labelsRanges.inputs}
            />

        </Popover>
    )
}

export default SelectedRange;