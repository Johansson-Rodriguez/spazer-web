import React from 'react';
import { makeStyles, Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    eventScheduleItem: {
        background: theme.palette.primary.main,
        color: 'black',
        width: '100%',
        height: '100%',
        cursor: 'pointer',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        textAlign: 'center'
    }
}))

const ContentCalendar = ({ event }) => {

    const classes = useStyles();

    return (
        <div
            // onClick={() => openDialog(eventInfo.event)}
            className={classes.eventScheduleItem}>
            <Typography variant={'h5'} className={classes.text}>
                Tiene{' '}
                {event.extendedProps.times
                    ? event.extendedProps.times.length
                    : 0}{' '}
                reservas
            </Typography>
        </div>
    )
}

export default ContentCalendar;