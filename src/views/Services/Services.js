import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core';
import { useGet } from 'hooks/useGet';
import { Loading, Table } from 'components';
import Buttons from 'components/Button';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(4)
    },
    contentButtons: {
        display: 'flex'
    },
    progress: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center'
    }
}));

const Services = () => {

    const classes = useStyles();

    const [loading, services] = useGet('/service/getAll');

    const history = useHistory();

    const handleClickEdit = ({row}) => history.push(`/services/edit/${row._id}`)

    return (
        <div className={classes.root}>
            {
                loading ? (
                    <Loading />
                ) : (
                    <>
                        <Buttons
                            title={services.services.length}
                            subtitle="Servicios"
                            type="primary"
                        />
                        <Table
                            data={services.services}
                            type="servicio"
                            showDelete={false}
                            onClickEdit={handleClickEdit}
                        />
                    </>
                )
            }
        </div>
    )
}

export default Services;