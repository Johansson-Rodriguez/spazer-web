import { Loading } from 'components';
import { useGet } from 'hooks/useGet';
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { Form } from 'views/FeaturesSettings/Sections'

const Edit = () => {

    const { id } = useParams();
    const [loading, features] = useGet(`/feature/${id}`);
    const [feature, setFeature] = useState({
        name: '',
        description: ''
    })

    useEffect(() => {

        if(!loading) {
            setFeature(features.feature)
        }
    }, [loading])

    return (
        <>

            {
                loading && (
                    <Loading />
                )
            }

            {
                !loading && (
                    <Form title="Editar Caracteristica" feature={feature}/>
                )
            }
        </>
    )
}

export default Edit