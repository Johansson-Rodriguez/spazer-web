import React from 'react'
import { makeStyles } from '@material-ui/styles';
import { Grid, Typography, TextField } from '@material-ui/core';
import { Button, Container } from 'components';
import { MultipleSelect } from 'views/FeaturesSettings/Components';

const useStyles = makeStyles(theme => ({
    name: {
        marginTop: theme.spacing(1),
        color: 'black'
    },
    containerButton: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: 20,
        width: '100%'
    },
    inputsContainer: {
        padding: 5
    }
}));

const Form = ({ title, feature }) => {

    const classes = useStyles();

    return (
        <>
            <Grid container>
                <Grid item md={6} sm={6} xs={12}>
                    <Typography className={classes.name} variant="h4">
                        {title}
                    </Typography>
                </Grid>
            </Grid>
            <Container>
                <div className={classes.inputsContainer}>
                    <TextField
                        fullWidth
                        type="text"
                        label="Nombre"
                        margin="dense"
                        name="name"
                        // onChange={handleChange}
                        required
                        value={feature.name}
                        variant="outlined"
                    />
                </div>
                <div className={classes.inputsContainer}>
                    <TextField
                        fullWidth
                        type="text"
                        label="Descripción"
                        margin="dense"
                        name="name"
                        // onChange={handleChange}
                        required
                        multiline
                        value={feature.description}
                        variant="outlined"
                    />
                </div>
                <div className={classes.inputsContainer}>
                    <MultipleSelect title="Categorias a la que pertenece" />
                </div>

            </Container>
            <div className={classes.containerButton}>
                <Button
                    subtitle="Guardar"
                    type="primary"
                />
            </div>
        </>
    )
}

export default Form;