import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Create, Edit } from './Views';
import PropTypes from 'prop-types'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(4)
    }
}));

const FeaturesSetttings = ({ mode }) => {

    const classes = useStyles();

    return (
        <div className={classes.root}>
            {
                mode === "create" && (<Create />)
            }

            {
                mode === "edit" && (<Edit />)
            }
        </div>
    )
}

FeaturesSetttings.propTypes = {
    mode: PropTypes.string.isRequired
}

export default FeaturesSetttings;