import React from 'react';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';

const animatedComponents = makeAnimated();

const styles = {
    option: (provided, state) => ({
        ...provided,
        zIndex: 9999
    })
}

const MultipleSelect = ({ title }) => {

    const options = [
        { value: 'chocolate', label: 'Chocolate' },
        { value: 'strawberry', label: 'Strawberry' },
        { value: 'vanilla', label: 'Vanilla' }
    ]

    return (
        <div>
            <Select
                closeMenuOnSelect={false}
                components={animatedComponents}
                isMulti
                options={options}
                styles={styles}
                placeholder={title}
            />
        </div>
    )
}

export default MultipleSelect;